﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pdfInterface.processingText
{
    public class MyDate
    {
        public MyDate(int _year, int _month, int _day)
        {
            month = _month;
            year = _year;
            day = _day;

            properDateTime = new DateTime(year, month, day);

        }

        public int month { get; set; }
        public int year { get; set; }
        public int day { get; set; }

        public DateTime properDateTime { get; set; }
    }
}
