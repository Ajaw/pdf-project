﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pdfInterface.processingText.ProcessedDates
{
    public class ProcessedDate
    {

        public DateTime beginDate { get; set; }
        public DateTime endDate { get; set; }
        public bool ignoreWeekDays { get; set; }

    }
}
