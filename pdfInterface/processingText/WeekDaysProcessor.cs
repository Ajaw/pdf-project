﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pdfInterface.processingText
{
    public class WeekDaysProcessor
    {

        public WeekDaysProcessor()
        {
            weekDays = new List<int>();
            for (int i = 1; i <= 7; i++)
            {
                weekDays.Add(i);
            }
        }

        public WeekDaysProcessor(IEnumerable<int> first, IEnumerable<int> second)
        {
            var firstList = first.ToList() ;
            var secondList = second.ToList();
            firstList.AddRange(secondList);
            weekDays = firstList;
        }


        public WeekDaysProcessor(IEnumerable<char> firstBatch, IEnumerable<char> secondBatch)
        {

            var first = firstBatch.ToList();
            var second = secondBatch.ToList();
            weekDays = new List<int>();
            if (secondBatch.Count() == 0)
            {
                foreach (var item in first)
                {
                    weekDays.Add(int.Parse(item.ToString()));
                }
            }
            else
            {
                int beginRange = int.Parse(first.Last().ToString());
                int closeRange = int.Parse(second.First().ToString());

                first.RemoveAt(first.Count() - 1);
                second.RemoveAt(0);
                for (int i = beginRange; i <= closeRange; i++)
                {
                    weekDays.Add(i);
                }
                foreach (var item in first)
                {
                    weekDays.Add(int.Parse(item.ToString()));
                }
                foreach (var item in second)
                {
                    weekDays.Add(int.Parse(item.ToString()));
                }
            }
        }

        public List<int> weekDays { get; set; }

    }
}
