﻿using Sprache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pdfInterface.processingText
{
    public class DateParser
    {



        public List<Expression> parseText(string text)
        {
            try
            {



                Parser<IEnumerable<string>> dates =
                Stripper.parserRest.Or(Stripper.connectionStringParser).XMany();


                List<string> finalData = new List<string>();
                var date = dates.Parse(text);
                foreach (var item in date)
                {
                    try
                    {

                        var blodData = item + ";";
                        finalData.Add(blodData);
                    }
                    catch (Exception)
                    {

                        finalData.Add(item);
                    }
                }
                Console.WriteLine(date);

                Parser<Expression> parser = Grammar.comabiner;
                List<Expression> connectionDates = new List<Expression>();
                foreach (string my_data in finalData)
                {
                    try
                    {
                        connectionDates.Add(parser.Parse(my_data));
                    }
                    catch (Exception ex)
                    {

                        connectionDates.Add(Grammar.allOptions.Parse(my_data));
                    }
                  
                }
             
                foreach (var item in connectionDates)
                {
                    item.processDates();
                }
                Console.WriteLine("test");
                return connectionDates;



            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            return new List<Expression>();
        }

    }

    static class Stripper
    {




        public static Parser<string> connectionStringParser =
          from first in Parse.Char('~')
          from rest in Parse.CharExcept(';').XMany().Text()
          from close in Parse.Char(';')
          select rest;

        public static Parser<string> parserRest =
            from first in Parse.WhiteSpace
            from rest in Parse.CharExcept(';').XMany().Text()
            from close in Parse.Char(';')
            select rest;

        public static Parser<string> commaParsing =
            from first in Parse.WhiteSpace
            from rest in Parse.CharExcept(',').XMany().Text()
            from close in Parse.WhiteSpace
            select rest;

        public static Parser<string> whiteSpace =
          from first in Parse.WhiteSpace
          from rest in Parse.CharExcept(',').XMany().Text()
          from close in Parse.WhiteSpace
          select rest;

        public static Parser<IEnumerable<string>> praseSemis =
            commaParsing.XMany();

        //    public static Parser<int> parseMonth =
        //        from mothText in Parse.

        //    public static Parser<int> parseDay =
        //        from day in Parse.Numeric.XMany().Text()
        //        select int.Parse(day);

        //    public static Parser<int> MonthGrammar =
        //        Parse.Chars("I")
        //        .Return(1)
        //        .Or(Parse.Chars("II")
        //            .Return(2))
        //            .Or(Parse.Chars("III")
        //            .Return(3))
        //             .Or(Parse.Chars("IV")
        //            .Return(4))
        //                 .Or(Parse.Chars("V")
        //            .Return(5))
        //                  .Or(Parse.Chars("VI")
        //            .Return(6))
        //                      .Or(Parse.Chars("VII")
        //            .Return(7))
        //                       .Or(Parse.Chars("VIII")
        //            .Return(8))
        //                           .Or(Parse.Chars("IX")
        //            .Return(9))
        //                            .Or(Parse.Chars("X")
        //            .Return(10))
        //                                .Or(Parse.Chars("XI")
        //            .Return(11))
        //                                 .Or(Parse.Chars("XII")
        //            .Return(12));

        //    public static Parser<IEnumerable<DateTime>> dateTimeMonth =
        //           (from day in parseDay
        //            from moth in parseMonth
        //            from end in Parse.WhiteSpace
        //            select new List<DateTime> { new DateTime(2017, moth, day) }).Or(
        //           from day in parseDay
        //           from moth in parseMonth
        //           from possible_var in Parse.Char('-')
        //           from day_2 in parseDay
        //           from month_2 in parseMonth
        //           select new List<DateTime>
        //           {
        //               new DateTime(2017,moth,day),
        //               new DateTime(2017, month_2,day_2)

        //           }
        //           );



        //    public static Parser<string> parseSingleDate =
        // from day in parseDay.
        // from moth in parseMonth
        // from possible_var in Parse.Char('-')
        // from day_2 in parseDay
        // from month_2 in parseMonth
        // select new Period()


        // from
        // .Then(from month in Parse.Char('-')).Then(parseDay)
        // select month)
        // .XOr()
        //}
    }

    public static class Grammar
    {

        public static DateTime beginDate;
        public static DateTime endDate;

        private static Parser<int> parseDay =
          from day in Parse.Numeric.XMany().Text()

          select int.Parse(day);

        private static Parser<int> MonthGrammar =
            Parse.String("XII")
            .Return(12)
            .Or(Parse.String("XI")
                .Return(11))
                .Or(Parse.String("X")
                .Return(10))
                 .Or(Parse.String("IX")
                .Return(9))
                     .Or(Parse.String("VIII")
                .Return(8))
                      .Or(Parse.String("VII")
                .Return(7))
                          .Or(Parse.String("VI")
                .Return(6))
                           .Or(Parse.String("V")
                .Return(5))
                               .Or(Parse.String("IV")
                .Return(4))
                                .Or(Parse.String("III")
                .Return(3))
                                    .Or(Parse.String("II")
                .Return(2))
                                     .Or(Parse.String("I")
                .Return(1));

        //private static Parser<int> MonthGrammar =
        //    Parse.Chars("I")
        //    .Return(1);

        public static Parser<IEnumerable<MyDate>> dateTimeMonth =

               (from day in parseDay
                from moth in parseMonth.Optional()
                let cond = moth.GetOrElse(0)

                from possible_var in Parse.Char('-')
                from day_2 in parseDay
                from month_2 in parseMonth


                select cond == 0 ? new List<MyDate>
               {

                   new MyDate(2017,  month_2 ,day),
                   new MyDate(2017, month_2,day_2)


               } :
             new List<MyDate>
                {
                   new MyDate(2017, moth.Get() ,day),
                    new MyDate(2017, month_2,day_2)

                }

               ).Or(
                (from day in parseDay
                 from moth in parseMonth
                 select new List<MyDate> { new MyDate(2017, moth, day) }))
                 .Or(

                   from b in Parse.Return(0)

                   select new List<MyDate> {new MyDate(2017,12,10),
                       new MyDate(2017,3,10) });


        private static Parser<int> parseMonth =
            from space in Parse.WhiteSpace
            from mothText in MonthGrammar
            select mothText;

        private static Parser<IEnumerable<int>> daysOfTheWeek =

                

            (from space in Parse.WhiteSpace.Optional()
             from firstNum in Parse.Numeric.XMany()
             from spacer in Parse.Char('-').Optional()
             from anotherNumbers in Parse.Numeric.XMany()

             select new WeekDaysProcessor(firstNum, anotherNumbers).weekDays).Or(
                  from space in Parse.WhiteSpace.Optional()
                  select new WeekDaysProcessor().weekDays);

        public static Parser<Expression> comabiner =
                (
          
                from space in Parse.WhiteSpace.Optional()
                from first in dateTimeMonth
                from weekdays in daysOfTheWeek
                from gap in Parse.WhiteSpace
                from slash in Parse.Char('/')
                from gap2 in Parse.WhiteSpace.Optional()
                from to_subtract in blobParser.Until(Parse.Char(';'))
                select new Expression { main = first.ToList(), to_subtract = to_subtract.ToList(), WeekDays = weekdays.ToList() }).Or(
                    from space in Parse.WhiteSpace.Optional()
                    from first in dateTimeMonth
                    from weekdays in daysOfTheWeek
                    from gap in Parse.WhiteSpace
                    from slash in Parse.Char('+')
                    from gap2 in Parse.WhiteSpace.Optional()
                    from to_add in blobParser.Until(Parse.Char(';'))
                    select new Expression { main = first.ToList(), to_add = to_add.ToList(), WeekDays = weekdays.ToList() }
                   ).Or(
                    from space in Parse.WhiteSpace.Optional()
                    from first in dateTimeMonth
                    from weekdays in daysOfTheWeek
                    select new Expression
                    {
                        main = first.ToList(),
                        WeekDays = weekdays.ToList()
                    });

        public static Parser<Expression> allOptions =

            (from space in Parse.WhiteSpace.Optional()
             from first in dateTimeMonth
             from weekdays in daysOfTheWeek
             from gap in Parse.WhiteSpace
             from slash in Parse.Char('+')
             from gap2 in Parse.WhiteSpace.Optional()
             from to_add in blobParser.Until(Parse.Char('/'))
             from gap3 in Parse.WhiteSpace.Optional()
             from to_subtract in blobParser.Until(Parse.Char(';'))
             select new Expression { main = first.ToList(), to_add = to_add.ToList(), to_subtract = to_subtract.ToList(), WeekDays = weekdays.ToList() });




        public static Parser<DateBlob> blobParser = 
        (         

             from date in dateTimeMonth
        from possibleComma in Parse.Char(',').Optional()
             from gap2 in Parse.WhiteSpace.Optional()
             select new DateBlob(date.ToList())
        );

        //  public static Parser<Expression> comabiner =
        //from first in dateTimeMonth
        //from weekdays in daysOfTheWeek
        //from gap in Parse.WhiteSpace.Optional()          
        //select new Expression { main = first.ToList() };


    }
}
