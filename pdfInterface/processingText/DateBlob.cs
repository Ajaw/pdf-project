﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pdfInterface.processingText
{
    public class DateBlob
    {

        public DateBlob(List<MyDate> data)
        {
            dateList = data;
        }

        public List<MyDate> dateList { get; set; }
        

    }
}
