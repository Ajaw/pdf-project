﻿using Sprache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pdfInterface.processingText.connectionNumber
{
    public static class ConnectionNumberParser
    {

        public static Parser<string> connectionNumberParser =
            from first in Parse.String("KS - OsP").Or(Parse.String("KS - Os"))
            from space in Parse.LineTerminator
            from connection_number in Parse.LetterOrDigit.Many()
            from rest in Parse.AnyChar
            select new string(connection_number.ToArray());

    }
}
