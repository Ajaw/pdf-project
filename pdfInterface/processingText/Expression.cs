﻿using pdfInterface.processingText.ProcessedDates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pdfInterface.processingText
{
    public class Expression
    {
        public List<MyDate> main { get; set; }

        public List<DateBlob> to_subtract { get; set; }

        public List<DateBlob> to_add { get; set; }

        public List<int> WeekDays { get; set; }

        public List<ProcessedDate> datesToInsert { get; set; }

        

        public void processDates()
        {         
            datesToInsert = new List<ProcessedDate>();
            ProcessedDate _pD = new ProcessedDate();
            try
            {

            
            if (main.Count==2)
            {
                DateTime currentDate = main[0].properDateTime;
                    if (to_subtract!=null)
                    {
                        foreach (var item in to_subtract)
                        {
                            if (item.dateList.Count == 2)
                            {
                                _pD = new ProcessedDate();
                                _pD.beginDate = currentDate;
                                _pD.endDate = item.dateList[0].properDateTime.Subtract(TimeSpan.FromDays(1));
                                currentDate = this.getCorrectDate(item.dateList[1].properDateTime.AddDays(1));
                                datesToInsert.Add(_pD);
                            }
                            else
                            {
                                _pD = new ProcessedDate();
                                _pD.beginDate = currentDate;
                                _pD.endDate = this.setBeforeMidnight(item.dateList[0].properDateTime.Subtract(TimeSpan.FromDays(1)));
                                currentDate = this.setDateMidnight(item.dateList[0].properDateTime.AddDays(1));
                                datesToInsert.Add(_pD);
                            }
                        }
                        _pD = new ProcessedDate();
                        _pD.beginDate = currentDate;
                        _pD.endDate = main[1].properDateTime;
                        datesToInsert.Add(_pD);
                    }
                    else
                    {
                        _pD = new ProcessedDate();
                        _pD.beginDate = currentDate;
                        _pD.endDate = main[1].properDateTime;
                        datesToInsert.Add(_pD);
                    }
              
                if (to_add!=null)
                {
                    foreach (var item in to_add)
                    {
                        if (item.dateList.Count == 2)
                        {
                            _pD = new ProcessedDate();

                            _pD.beginDate = item.dateList[0].properDateTime;
                            _pD.endDate = item.dateList[1].properDateTime;
                            _pD.ignoreWeekDays = true;
                            datesToInsert.Add(_pD);
                        }
                        else
                        {
                            _pD = new ProcessedDate();
                            _pD.ignoreWeekDays = true;
                            _pD.beginDate = this.setDateMidnight(item.dateList[0].properDateTime);
                            _pD.endDate = this.setBeforeMidnight(item.dateList[0].properDateTime);
                            datesToInsert.Add(_pD);
                        }
                    }
                }
               
            }
            else if (main.Count==1)
            {
                _pD = new ProcessedDate();
                _pD.beginDate = this.setDateMidnight(main[0].properDateTime); 
                _pD.endDate = this.setBeforeMidnight(main[0].properDateTime);
                datesToInsert.Add(_pD);
            }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        
        private DateTime setDateMidnight(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
            
        }
        private DateTime setBeforeMidnight(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);

        }

        private DateTime getCorrectDate(DateTime date)
        {
            date.AddDays(1);
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);

        }



    }
}
