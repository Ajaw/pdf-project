﻿using pdfInterface.processingText.connectionNumber;
using RestClientRozklad.db_model;
using RestClientRozklad.db_model.connectionData;
using Sprache;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace pdfInterface.processingText
{
    public class TextProcessor
    {

        public void ExtractFromPdfPage(PageData myData, List<dbStationsOnConnection> stationList,dbScheduleVersion version)
        {
            this.version_id = version.id;
            foreach (var text in myData.textData)
            {
                try
                {

              
                    if (text.detail.Contains("KS - Os"))
                    {
                        Parser<string> parser = ConnectionNumberParser.connectionNumberParser;
                        var connectionNumber = parser.Parse(text.detail);
                        ExtractSingleStations(text.fullData,text.departureHour, stationList,connectionNumber);
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine("error");
                }

            }
        }

        private int version_id;

        private int endPostion;
        private int beginPostion;

        private TrainDbContext context;

        private dbScheduleVersion version;


        public void ExtractSingleStations(string text,string departureTime, List<dbStationsOnConnection> stationList,string connectionNumber)
        {
           
            String sub;
            String stripped = text;
            List<String> extractedStations = new List<string>();
            List<Expression> connectionDateRange = new List<Expression>();

            List<dbConnectionToHour> connectionToHours = new List<dbConnectionToHour>();
            if (extractedStations.Count == stationList.Count - 1)
            {
                if (extractedStations.First() == stationList[1].station.stationName
                    && extractedStations.Last() == stationList.Last().station.stationName)
                {
                    Console.WriteLine("test");
                }
            }
            List<TempAHour> arrivalList = new List<TempAHour>();
            TempAHour tempHours = new TempAHour();
            
            tempHours.station = stationList.First().station;
            TimeSpan depHour = TimeSpan.Parse(departureTime);
            DateTime now = DateTime.Now;



            var tempDate = new DateTime(now.Year, now.Month, now.Day, depHour.Hours, depHour.Minutes, 0);
            tempHours.arrivalHour = tempDate;
            arrivalList.Add(tempHours);
            try
            {
              


                var indexes = Regex.Matches(text, ":").Cast<Match>().Select(m => m.Index);

                var terminator_pos = text.IndexOf("~");

                indexes.Select(t => t < terminator_pos);
                string hours = String.Empty;
                string minutes = String.Empty;
                int currentPos = 0;
                string stationName = String.Empty;
                int last = 0;
                int size = indexes.Count();

                var semiPos = text.LastIndexOf(';') + 1;

                text = text.Replace(Environment.NewLine, ",");
                try
                {
                    if (terminator_pos > 0)
                    {

                  
                    DateParser parser = new DateParser();                    
                    semiPos = text.LastIndexOf(';') + 1;
                    // text =  text.Insert(semiPos, ",");
                    sub = text.Substring(terminator_pos, (semiPos) - (terminator_pos));
                    stripped = text.Replace("\n", ",");
                    stripped = stripped.Remove(terminator_pos, semiPos - terminator_pos);
                    stripped = stripped.Replace(sub, "");

                     connectionDateRange = parser.parseText(sub);
                    }
                    if (connectionDateRange.Count==0)
                    {
                        Expression tempEx = new Expression();
                        ProcessedDates.ProcessedDate date = new ProcessedDates.ProcessedDate();
                        date.beginDate = new DateTime(2017, 12, 10);
                        date.endDate = new DateTime(2018, 3, 10);
                        List<int> tempWeekDays = new List<int>();
                        for (int i = 1; i <= 7; i++)
                        {
                            tempWeekDays.Add(i);
                        }
                        tempEx.WeekDays = tempWeekDays;
                        tempEx.datesToInsert = new List<ProcessedDates.ProcessedDate>();
                        tempEx.datesToInsert.Add(date);
                        connectionDateRange.Add(tempEx);
                    }
                    
                    indexes = Regex.Matches(stripped, ":").Cast<Match>().Select(m => m.Index);
                    Console.WriteLine("test");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                //stripped = stripped.Replace("\n", String.Empty);
                //stripped = stripped.Replace("\r", String.Empty);
                //stripped = stripped.Replace("\t", String.Empty);
                foreach (var index in indexes)
                {

                    try
                    {

                        hours = extractHours(stripped, index);


                        if (currentPos != 0)
                        {
                            last = beginPostion;
                        }

                        if ((currentPos != size))
                        {
                            stationName = stripped.Substring(last, endPostion - last);
                        }


                        minutes = extractMinutes(stripped, index);


                        var timeS = hours.ToString() + ":" + minutes.ToString();

                        var time = TimeSpan.Parse(timeS);
                        currentPos++;



                        var result = new string(stationName.Where(c => Char.IsLetter(c) || Char.IsWhiteSpace(c) || c == ' ' || c == '-').ToArray());

                        Regex regex = new Regex(@"\s");
                        string[] bits = regex.Split(result);

                        result = String.Empty;
                        bool first = true;
                        foreach (var item in bits)
                        {
                            if (!(item == String.Empty) && item.Count()>2)
                            {
                                string temp = String.Empty;
                                if (!first)
                                {
                                    temp = new string(item.Where(a => Char.IsLetter(a) || a == '-').ToArray());
                                    result += " " + temp;
                                }
                                else
                                {
                                    temp = new string(item.Where(a => Char.IsLetter(a) || a == '-').ToArray());
                                    result += temp;
                                    first = false;
                                }

                            }
                        }

                        Console.WriteLine("" + result + " " + timeS + "");
                        extractedStations.Add(result);
                        TempAHour arrivalHour = new TempAHour();
                        DateTime date = DateTime.Now;
                    
                    

                        date = new DateTime(date.Year, date.Month, date.Day, time.Hours, time.Minutes, 0);
                        var tmp = stationList.Where(a => a.station.stationName.ToLower().Replace(" ", string.Empty) == result.ToLower().Replace(" ", string.Empty)).FirstOrDefault();
                        if (tmp!=null)
                        {
                            arrivalHour.station = tmp.station;
                        }
                        else
                        {
                            return;
                        }
                       
                       
                        arrivalHour.arrivalHour = date;
                        arrivalList.Add(arrivalHour);
                      
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        

            if (extractedStations.Count == stationList.Count-1)
            {
             
                if (extractedStations.First() == stationList[1].station.stationName
                    && extractedStations.Last() == stationList.Last().station.stationName)
                {
                    context = new TrainDbContext();
                    version = context.ScheduleVersion.Where(a => a.id == version_id).FirstOrDefault();
                 
                    dbConnectionToHour connToHour = new dbConnectionToHour();
                   
                    List<ArrivalHours> arrival = new List<ArrivalHours>();
                   
                    connToHour.connectionID = stationList.First().connectionID;
                    connToHour.version = version;
                    context.StopsHoursToConnection.Add(connToHour);
                    context.SaveChanges();
                    foreach (var item in arrivalList)
                    {
                        ArrivalHours tmp = new ArrivalHours();
                        tmp.departureTime = item.arrivalHour;
                        tmp.connectionToHour = context.StopsHoursToConnection.Where(a => a.ID == connToHour.ID).First();
                        tmp.newStationID = item.station.ID;
                        arrival.Add(tmp);
                        context.StopHours.Add(tmp);
                        context.SaveChanges();
                    }
                  //  connToHour.ArrivalHours = arrival;
                    
                 
                    var connectionData = stationList[0].connection;
                    addConnectionInfoToDb(connectionDateRange,connectionNumber,connectionData,connToHour);
                   
                    
                }
            }

            Console.WriteLine("end");



        }

        private void addConnectionInfoToDb(List<Expression> hours, string connectionNumber,dbConnection connection,dbConnectionToHour hour)
        {
           
           
            foreach (var item in hours)
            {
                
                try
                {

              
                    List<dbConnectionAvailability> avalibilyList = new List<dbConnectionAvailability>();
              
                    
                    foreach (var data in item.datesToInsert)
                    {
                        dbConnectionAvailability ava = new dbConnectionAvailability();
                        if (data.beginDate.Month <= version.dateTo.Month)
                        {
                            var tempDate = data.beginDate;
                            ava.beggingDate = new DateTime(version.dateTo.Year, tempDate.Month, tempDate.Day);
                           
                        }
                        else
                        {
                            ava.beggingDate = data.beginDate;                       
                        }
                        if (data.endDate.Month <= version.dateTo.Month)
                        {
                            var tempDate = data.endDate;
                            ava.endDate = new DateTime(version.dateTo.Year, tempDate.Month, tempDate.Day);
                        }
                        else
                        {
                            ava.endDate = data.endDate;
                        }                                                        
                        ava.ignoreWeekDays = data.ignoreWeekDays;
                        avalibilyList.Add(ava);
                        context.ConnectionAvailability.Add(ava);
                   
                    
                    }
                    dbConnectionMapping mapping = this.mapWeekDays(item.WeekDays);
                    mapping.connection_number = connectionNumber;
                    mapping.was_processed = false;
                    mapping.connectionMapping = new List<dbConnectionAvailability>();
                    mapping.connectionMapping.AddRange(avalibilyList);
                    connection = context.Connections.Where(a => a.ID == connection.ID).First();
                    mapping.connection = connection;
                    mapping.connectionToHour = hour;
                    mapping.version = this.version;
                    context.ConnectionMapping.Add(mapping);
               
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
            }
            context.SaveChanges();
          


        }

        private dbConnectionMapping mapWeekDays(List<int> weekdays)
        {
            dbConnectionMapping mapping = new dbConnectionMapping();
            foreach (var item in weekdays)
            {
                switch (item)
                {
                    case 1:
                        mapping.monday = true;
                        break;
                    case 2:
                        mapping.tuesday = true;
                        break;
                    case 3:
                        mapping.wenesday = true;
                        break;
                    case 4:
                        mapping.thursday = true;
                        break;
                    case 5:
                        mapping.friday = true;
                        break;
                    case 6:
                        mapping.saturday = true;
                        break;
                    case 7:
                        mapping.sunday = true;
                        break;
                    default:
                        break;
                }
            }
            return mapping;
        }


        private string extractHours(string text, int index)
        {
            int hour;
            string textHour = String.Empty;
            for (int i = 1; i <= 2; i++)
            {
                var currentText = text[index - i];
                var value = (byte)text[index - i];

                if (!value.Equals(160))
                {
                    textHour += currentText.ToString();
                    endPostion = index - i;
                }
            }
            return new string(textHour.Reverse().ToArray());
        }

        private string extractMinutes(string text, int index)
        {
            int hour;
            string textHour = String.Empty;
            for (int i = 1; i <= 2; i++)
            {
                var currentText = text[index + i];
                var value = (byte)text[index + i];
                if (!currentText.Equals(",") && !value.Equals(160))
                {
                    textHour += currentText.ToString();
                    beginPostion = index + i;
                }
            }
            return textHour;
        }

    }
}
