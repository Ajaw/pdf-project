﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestClientRozklad;
using RestClientRozklad.db_model;
using AngleSharp.Parser;
using AngleSharp.Parser.Html;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace pdfInterface.pdfDownloading
{
    class pdfDownloader
    {

        private int month=12;
 

        private HashSet<dbStation> stationsOnConnections;

        private client myclient;

        public pdfDownloader()
        {
            TrainDbContext context = new TrainDbContext();

            context.Configuration.LazyLoadingEnabled = false;

            var connections = context.Connections.ToList();

            var stops = context.Stops.ToList();

            var stations = context.Stations.ToList();

             stationsOnConnections = new HashSet<dbStation>();

            foreach (var item in connections)
            {
                try
                {





                    var first = item.StopsOnRoute.First();
                    var last = item.StopsOnRoute.Last();

                    stationsOnConnections.Add(first.station);
                    stationsOnConnections.Add(last.station);

                }
                catch (Exception)
                {
                    Console.WriteLine("no connecitons");

                }
            }
            myclient = new client();

            foreach (var item in stationsOnConnections)
            {
                try
                {

               

                downloadRequiredPDFS(item.stationName);

                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
            }

        }


        private void downloadRequiredPDFS(string stationName)
        {
            var response =  myclient.makeGetRequest("https://portalpasazera.pl/Plakaty");

            var url = parseToGetToken(response);

            var stationID = getStationID(stationName);


            var postJSON = myclient.makePostRequest("https://portalpasazera.pl/Plakaty/PlakatyRead", "sort=&page=1&pageSize=10&group=&filter=&stacjaID="+stationID.ToString()+"");

            var items = JsonConvert.DeserializeObject<RootPosterJSON>(postJSON);

            var regex = new Regex(Regex.Escape("#=ID#"));

            var poster_id = items.Data.Where(a => a.WaznyOd.Month == month && a.Odjazdy == true && a.Format == "WS").First().ID;

            url = regex.Replace(url, poster_id.ToString(), 1);

            url = "https://portalpasazera.pl/" + url;

            myclient.downloadPDFfileFromStream(url, stationName);


            Console.WriteLine("test");
        }

        private int getStationID(string stationName)
        {
            var response = myclient.makeGetRequestForPoster("https://portalpasazera.pl/Kontrolki/StacjeFiltrReadCache?formatyPlakatow=%5B%22WS%22%2C%22WG%22%2C%22WSR%22%2C%22WGR%22%5D&text=" + stationName + "&zarzadKod=51&filter%5Blogic%5D=and&filter%5Bfilters%5D%5B0%5D%5Bvalue%5D=" + stationName + "&filter%5Bfilters%5D%5B0%5D%5Bfield%5D=Nazwa&filter%5Bfilters%5D%5B0%5D%5Boperator%5D=startswith&filter%5Bfilters%5D%5B0%5D%5BignoreCase%5D=true");

            var items = JsonConvert.DeserializeObject<List<stationJSON>>(response);

            return items.First().ID;

        }

        private string parseToGetToken(string response)
        {
            HtmlParser parser = new HtmlParser();

            var documnet = parser.Parse(response);

            var onClicks  = documnet.GetElementsByTagName("a").Where(a => a.HasAttribute("onClick")).ToList();

            var scriptStuff = documnet.GetElementById("getPlakatTemplate").TextContent;

            var scripts = parser.Parse(scriptStuff);

            var links = scripts.GetElementsByTagName("a");

            var refs = links.Where(a => a.GetAttribute("onClick") == "zamknijOknoPlakatow()");

            return refs.Last().GetAttribute("href");

        }
       

      

    }
}
