﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pdfInterface.pdfDownloading
{
    public class stationJSON
    {
        public int ID { get; set; }
        public string Nazwa { get; set; }
        public string ObiektKod { get; set; }
        public string ZarzadKod { get; set; }
        public string ZakladKod { get; set; }
        public object SzerokoscGeograficzna { get; set; }
        public object DlugoscGeograficzna { get; set; }
        public object KrajKod { get; set; }
    }
}
