﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pdfInterface.pdfDownloading
{
    public class PosterJSON
    {
        public int ID { get; set; }
        public int RozkladID { get; set; }
        public string WersjaJezykowa { get; set; }
        public DateTime WaznyOd { get; set; }
        public DateTime WaznyDo { get; set; }
        public bool Odjazdy { get; set; }
        public bool Przyjazdy { get; set; }
        public string Format { get; set; }
        public object Opis { get; set; }
    }
}
