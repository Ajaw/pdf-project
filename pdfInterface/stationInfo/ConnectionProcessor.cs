﻿using RestClientRozklad.db_model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace pdfInterface.stationInfo
{
    public class ConnectionProcessor
    {

        public ConnectionProcessor()
        {
  
        }

      
        public RestClientRozklad.client client { get; set; }

        public List<dbConnection> getConnections()
        {
            using (var context = new TrainDbContext())
            {


                context.Configuration.ProxyCreationEnabled = true;
                context.Configuration.LazyLoadingEnabled = true;
                var station = context.Stations;
                context.Connections.Include(a => a.StopsOnRoute.Select(u => u.station)).Load();
                
                List<dbConnection> result = new List<dbConnection>();
                var resultList = context.Connections.ToList();
                for (int i = 0; i < context.Connections.ToList().Count; i++)
                {
                    result.Add(resultList[i]);
                }


                var list = context.Connections.ToList();
                return result;
            }

        }

        public dbStationsOnConnection getFirstStation(dbConnection connection)
        {
            return connection.StopsOnRoute.First();
        }


        public dbStationsOnConnection getLastStation(dbConnection connection)
        {
            return connection.StopsOnRoute.Last();
        }

    }
}
