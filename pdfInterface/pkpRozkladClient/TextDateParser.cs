﻿using pdfInterface.processingText;
using Sprache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pdfInterface.pkpRozkladClient
{
    public class FullDataParser
    {
        public List<Expression> parseText(string text)
        {
            try
            {



                Parser<IEnumerable<string>> dates =
                TextDateParser.parserRest.XMany();


                List<string> finalData = new List<string>();
                var date = dates.Parse(text);
                foreach (var item in date)
                {
                    try
                    {

                        var blodData = item + ";";
                        finalData.Add(blodData);
                    }
                    catch (Exception)
                    {

                        finalData.Add(item);
                    }
                }
                Console.WriteLine(date);

                Parser<Expression> parser = GrammarFull.comabiner;
                List<Expression> connectionDates = new List<Expression>();
                foreach (string my_data in finalData)
                {
                    try
                    {
                        connectionDates.Add(parser.Parse(my_data));
                    }
                    catch (Exception ex)
                    {

                        connectionDates.Add(Grammar.allOptions.Parse(my_data));
                    }

                }

                foreach (var item in connectionDates)
                {
                    item.processDates();
                }
                Console.WriteLine("test");
                return connectionDates;



            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            return new List<Expression>();
        }
    }

    static class TextDateParser
    {

        public static Parser<string> connectionStringParser =
          from first in Parse.Char('~')
          from rest in Parse.CharExcept(';').XMany().Text()
          from close in Parse.Char(';')
          select rest;

        public static Parser<string> parserRest =
            from first in Parse.WhiteSpace
            from rest in Parse.CharExcept(';').XMany().Text()
            from close in Parse.Char(';')
            select rest;

        public static Parser<string> commaParsing =
            from first in Parse.WhiteSpace
            from rest in Parse.CharExcept(',').XMany().Text()
            from close in Parse.WhiteSpace
            select rest;

        public static Parser<string> whiteSpace =
          from first in Parse.WhiteSpace
          from rest in Parse.CharExcept(',').XMany().Text()
          from close in Parse.WhiteSpace
          select rest;

        public static Parser<IEnumerable<string>> praseSemis =
            commaParsing.XMany();

    }

    public static class GrammarFull
    {

        private static Parser<int> parseDay =
          from day in Parse.Numeric.XMany().Text()
          from dot in Parse.Char('.')
          select int.Parse(day);

        private static Parser<int> MonthGrammar =
            Parse.String("Grudzień")
            .Return(12)
            .Or(Parse.String("Listopad")
                .Return(11))
                .Or(Parse.String("Październik")
                .Return(10))
                 .Or(Parse.String("Wrzesień")
                .Return(9))
                     .Or(Parse.String("Sierpień")
                .Return(8))
                      .Or(Parse.String("Lipiec")
                .Return(7))
                          .Or(Parse.String("Czerwiec")
                .Return(6))
                           .Or(Parse.String("Maj")
                .Return(5))
                               .Or(Parse.String("Kwiecień")
                .Return(4))
                                .Or(Parse.String("Marzec")
                .Return(3))
                                    .Or(Parse.String("Luty")
                .Return(2))
                                     .Or(Parse.String("Styczeń")
                .Return(1));

        //private static Parser<int> MonthGrammar =
        //    Parse.Chars("I")
        //    .Return(1);

        public static Parser<IEnumerable<MyDate>> dateTimeMonth =

               (from day in parseDay
                from moth in parseMonth.Optional()
                let cond = moth.GetOrElse(0)

                from possible_var in Parse.Char('-')
                from day_2 in parseDay
                from month_2 in parseMonth


                select cond == 0 ? new List<MyDate>
               {

                   new MyDate(2017,  month_2 ,day),
                   new MyDate(2017, month_2,day_2)


               } :
             new List<MyDate>
                {
                   new MyDate(2017, moth.Get() ,day),
                    new MyDate(2017, month_2,day_2)

                }

               ).Or(
                (from day in parseDay
                 from moth in parseMonth
                 select new List<MyDate> { new MyDate(2017, moth, day) }))
                 .Or(

                   from b in Parse.Return(0)

                   select new List<MyDate> {new MyDate(2017,6,11),
                       new MyDate(2017,9,2) });


        private static Parser<int> parseMonth =
            from space in Parse.WhiteSpace
            from mothText in MonthGrammar
            select mothText;

        private static Parser<int> WeekDayGrammar =
             Parse.String("Niedziela")
                .Return(7)
                    .Or(Parse.String("Sobota")
                .Return(6))
                           .Or(Parse.String("Piątek")
                .Return(5))
                               .Or(Parse.String("Czwartek")
                .Return(4))
                                .Or(Parse.String("Środa")
                .Return(3))
                                    .Or(Parse.String("Wtorek")
                .Return(2))
                                     .Or(Parse.String("Poniedziałek")
                .Return(1));

        private static Parser<int> ParseWeekDay =
            from weekDay in WeekDayGrammar
            from comma in Parse.Char(',').Optional()
            from space in Parse.WhiteSpace.Optional()
            select weekDay;


        private static Parser<IEnumerable<int>> daysOfTheWeek =
            (from space in Parse.WhiteSpace.Optional()
             from firstNum in ParseWeekDay.XMany()
             from spacer in Parse.Char('-').Optional()
             from anotherNumbers in ParseWeekDay.XMany()

             select new WeekDaysProcessor(firstNum, anotherNumbers).weekDays).Or(
                from b in Parse.AnyChar.Not()
                select new WeekDaysProcessor().weekDays);

        public static Parser<Expression> comabiner =
                (

                from space in Parse.WhiteSpace.Optional()
                from first in dateTimeMonth
                from weekdays in daysOfTheWeek
                from gap in Parse.WhiteSpace
                from slash in Parse.String("oprócz")
                from gap2 in Parse.WhiteSpace.Optional()
                from to_subtract in blobParser.Until(Parse.Char(';'))
                select new Expression { main = first.ToList(), to_subtract = to_subtract.ToList(), WeekDays = weekdays.ToList() }).Or(
                    from space in Parse.WhiteSpace.Optional()
                    from first in dateTimeMonth
                    from weekdays in daysOfTheWeek
                    from gap in Parse.WhiteSpace
                    from slash in Parse.String("także")
                    from gap2 in Parse.WhiteSpace.Optional()
                    from to_add in blobParser.Until(Parse.Char(';'))
                    select new Expression { main = first.ToList(), to_add = to_add.ToList(), WeekDays = weekdays.ToList() }
                   ).Or(
                    from space in Parse.WhiteSpace.Optional()
                    from first in dateTimeMonth
                    from weekdays in daysOfTheWeek
                    select new Expression
                    {
                        main = first.ToList(),
                        WeekDays = weekdays.ToList()
                    });

        public static Parser<Expression> allOptions =

            (from space in Parse.WhiteSpace.Optional()
             from first in dateTimeMonth
             from weekdays in daysOfTheWeek
             from gap in Parse.WhiteSpace
             from slash in Parse.String("także")
             from gap2 in Parse.WhiteSpace.Optional()
             from to_add in blobParser.Until(Parse.Char('/'))
             from gap3 in Parse.WhiteSpace.Optional()
             from to_subtract in blobParser.Until(Parse.Char(';'))
             select new Expression { main = first.ToList(), to_add = to_add.ToList(), to_subtract = to_subtract.ToList(), WeekDays = weekdays.ToList() });




        public static Parser<DateBlob> blobParser =
        (

             from date in dateTimeMonth
             from possibleComma in Parse.Char(',').Optional()
             from gap2 in Parse.WhiteSpace.Optional()
             select new DateBlob(date.ToList())
        );

        //  public static Parser<Expression> comabiner =
        //from first in dateTimeMonth
        //from weekdays in daysOfTheWeek
        //from gap in Parse.WhiteSpace.Optional()          
        //select new Expression { main = first.ToList() };


    }
}

