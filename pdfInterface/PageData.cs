﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static pdfInterface.MainWindow;

namespace pdfInterface
{
    public class PageData
    {
        public List<point> linesPositions { get; set; }
        public int pageNumber { get; set; }
        public List<ConnectionData> textData { get; set; }
    }
}
