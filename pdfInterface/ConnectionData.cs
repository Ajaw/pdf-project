﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pdfInterface
{
    public class ConnectionData
    {

        public string detail { get; set; }

        public string fullData { get; set; }

        public string departureHour { get; set; }

    }
}
