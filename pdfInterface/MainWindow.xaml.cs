﻿using Ghostscript.NET.Rasterizer;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using pdfInterface.pdfDownloading;
using pdfInterface.processingText;
using pdfInterface.stationInfo;
using RestClientRozklad.db_model;
using RestClientRozklad.db_model.connectionData;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.util;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace pdfInterface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport("opencvPDF.dll")]
        public static extern void getData(string filename ,out IntPtr arrayPtr, out int size);
 

        [StructLayout(LayoutKind.Sequential)]
        public struct point
        {
            [MarshalAs(UnmanagedType.I4)]
            public int xBegin;
            [MarshalAs(UnmanagedType.I4)]
            public int yBegin;
            [MarshalAs(UnmanagedType.I4)]
            public int xEnd;
            [MarshalAs(UnmanagedType.I4)]
            public int yEnd;

        };


        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct data
        {
            [MarshalAs(UnmanagedType.SafeArray)]
            public  point[]  array;
         

        };

        public MainWindow()
        {
            InitializeComponent();


             // pdfDownloader downloader = new pdfDownloader();




            ConnectionProcessor connProc = new ConnectionProcessor();
            var connections = connProc.getConnections();
            //connections.RemoveRange(0, 3);
            // var ints = new List<int>(){ 376,377,379,380,415,416};
            // var ints = new List<int>() { 391 };

            // connections = connections.Where(a => ints.Contains(a.ID)).ToList();

            List<PageData> pdfData = new List<PageData>();
            TrainDbContext context = new TrainDbContext();
            dbScheduleVersion version = new dbScheduleVersion();
            version.dateFrom = new DateTime(2017, 12, 10);
            version.dateTo = new DateTime(2018, 3, 10);
            context.ScheduleVersion.Add(version);

            context.SaveChanges();
           // context.Entry(version).State = System.Data.Entity.EntityState.Unchanged;
            foreach (var connection in connections)
                {
                    try
                    {
                        if (connection.StopsOnRoute.Count<1)
                        {
                            continue;
                        }
                        connection.StopsOnRoute =  connection.StopsOnRoute.OrderBy(a => a.stationNumberOnRoute).ToList();
                        var first = connProc.getFirstStation(connection);
                        var last = connProc.getLastStation(connection);


                        List<string> paths = convertPDFToImages("pdfs/"+first.station.stationName+".pdf");

                        string param = paths[0];

                        int currentPage = 1;
                        pdfData = new List<PageData>();
                        foreach (var path in paths)
                        {



                            var arrayValue = IntPtr.Zero;

                            var size = 0;

                            var list = new List<point>();

                            getData(path, out arrayValue, out size);



                            var dataEntrySize = Marshal.SizeOf(typeof(point));
                            for (var i = 0; i < size; i++)
                            {
                                var cur = (point)Marshal.PtrToStructure(arrayValue, typeof(point));
                                list.Add(cur);
                                arrayValue = new IntPtr(arrayValue.ToInt64() + dataEntrySize);
                            }

                            list = list.OrderBy(a => a.yBegin).ToList();

                            var current = list[1];
                            var compare = list[0];
                            var to_remove = new List<point>();
                            for (int i = 1; i < list.Count; i++)
                            {
                                current = list[i];
                                if (current.yBegin - compare.yBegin < 10)
                                {
                                    to_remove.Add(current);
                                }
                                else if (current.xEnd - current.xBegin < 500)
                                {
                                    to_remove.Add(current);
                                }
                                else
                                {
                                    compare = current;
                                }
                            }

                            foreach (var item in to_remove)
                            {
                                list.Remove(item);
                            }

                            pdfData.Add(new PageData { linesPositions = list, pageNumber = currentPage });
                            currentPage++;

                        }

                        PdfReader reader = new PdfReader("pdfs/" + first.station.stationName + ".pdf");
                        //var page = reader.GetPageN(3);
                        //var page_size = reader.GetPageSize(3);

                        var detailEnd = iTextSharp.text.Utilities.InchesToPoints(1.94f);
                        var detailBegin = iTextSharp.text.Utilities.InchesToPoints(1.19f);
                        var lengthDetail = detailEnd - detailBegin;



                        foreach (var pdfPage in pdfData)
                        {

                            int pageNumber = pdfPage.pageNumber;
                            var list = pdfPage.linesPositions;
                            pdfPage.textData = new List<ConnectionData>();
                            // List<string> data = new List<string>();
                            TextProcessor proc = new TextProcessor();
                            for (int i = 1; i < list.Count - 1; i++)
                            {
                                var first_item = list[i];
                                var second_item = list[i + 1];

                                RectangleJ rect = new RectangleJ(detailEnd, 842 - pixelsToPoints(second_item.yBegin),
                                    pixelsToPoints(764) - detailEnd, pixelsToPoints(second_item.yEnd - first_item.yBegin));
                                RectangleJ detailsRect = new RectangleJ(detailBegin, 842 - pixelsToPoints(second_item.yBegin),
                                    lengthDetail, pixelsToPoints(second_item.yEnd - first_item.yBegin));
                            RectangleJ arrivalHourRect = new RectangleJ(19, 842 - pixelsToPoints(second_item.yBegin),
                                39, pixelsToPoints(second_item.yEnd - first_item.yBegin));
                                RenderFilter[] filter = { new RegionTextRenderFilter(rect) };



                                ITextExtractionStrategy strategy;

                                strategy = new FilteredTextRenderListener(new LocationTextExtractionStrategy(), filter);

                                string test = PdfTextExtractor.GetTextFromPage(reader, pageNumber, strategy);

                                string detailPDF = extractDataFromPDF(reader, pageNumber, detailsRect);


                               string depHour = extractDataFromPDF(reader, pageNumber, arrivalHourRect);


                            ConnectionData conData = new ConnectionData { detail = detailPDF, fullData = test,departureHour = depHour };

                                pdfPage.textData.Add(conData);




                            }
                        
                        proc.ExtractFromPdfPage(pdfPage,connection.StopsOnRoute.ToList(),version);
                            Console.WriteLine();
                        }

                        Console.WriteLine();

                    }
                    catch (Exception ex)
                    {

                        Console.WriteLine(ex.Message);
                    }
                }
          
          


        }

        public string extractDataFromPDF(PdfReader reader, int pageNumber, RectangleJ rect)
        {
            RenderFilter[] filter = { new RegionTextRenderFilter(rect) };



            ITextExtractionStrategy strategy;

            strategy = new FilteredTextRenderListener(new LocationTextExtractionStrategy(), filter);

            string test = PdfTextExtractor.GetTextFromPage(reader, pageNumber, strategy);

            return test;
        }

        public List<string> convertPDFToImages(string filename)
        {
            int desired_x_dpi = 96;
            int desired_y_dpi = 96;

            string inputPdfPath = "tmp/temp.pdf";
            string outputPath = "tmp/";

            List<string> paths = new List<string>();

          

            File.Copy(filename, inputPdfPath,true);

            using (var rasterizer = new GhostscriptRasterizer())
            {
                rasterizer.Open(inputPdfPath);

                for (int i = 1; i <= rasterizer.PageCount; i++)
                {
                    var pageFilePath = System.IO.Path.Combine(outputPath, string.Format("Page-{0}.jpg", i));

                    var img = rasterizer.GetPage(desired_x_dpi, desired_y_dpi, i);
                    img.Save(pageFilePath, ImageFormat.Jpeg);

                    Console.WriteLine(pageFilePath);
                    paths.Add(pageFilePath);

                }
            }

            return paths;
        }

        public float pixelsToPoints(int number)
        {
            float fNumb = (float)number;
            float value = (float)(fNumb / 1.3333);
            Console.WriteLine(value);
            return value;
        }
    }
}
