#include <iostream>
#include <opencv2\opencv.hpp>
#include  "data.h"
#include <stdio.h>
#include <Windows.h>

using namespace std;
using namespace cv;

struct point
{
	int xBegin;
	int yBegin;
	int xEnd;
	int yEnd;
};



bool isEqual(const Vec4i& _l1, const Vec4i& _l2)
{
	Vec4i l1(_l1), l2(_l2);

	float length1 = sqrtf((l1[2] - l1[0])*(l1[2] - l1[0]) + (l1[3] - l1[1])*(l1[3] - l1[1]));
	float length2 = sqrtf((l2[2] - l2[0])*(l2[2] - l2[0]) + (l2[3] - l2[1])*(l2[3] - l2[1]));

	float product = (l1[2] - l1[0])*(l2[2] - l2[0]) + (l1[3] - l1[1])*(l2[3] - l2[1]);

	if (fabs(product / (length1 * length2)) < cos(CV_PI / 30))
		return false;

	float mx1 = (l1[0] + l1[2]) * 0.5f;
	float mx2 = (l2[0] + l2[2]) * 0.5f;

	float my1 = (l1[1] + l1[3]) * 0.5f;
	float my2 = (l2[1] + l2[3]) * 0.5f;
	float dist = sqrtf((mx1 - mx2)*(mx1 - mx2) + (my1 - my2)*(my1 - my2));

	if (dist > max(length1, length2) * 0.5f)
		return false;

	return true;
}


extern "C" __declspec(dllexport) void __cdecl getData(const char *filename,point **aData, int *length) {
	//string filename = "file-page3.jpg";


	

	Mat src = imread(filename);

	if (!src.data)
	{
		cerr << "Problem loading image" << endl;
	}

	//imshow("src", src);

	//Mat gray;

	//if (src.channels() ==3)
	//{
	//	cvtColor(src, gray, CV_BGR2GRAY);
	//}
	//else
	//{
	//	gray = src;
	//}

	//imshow("gray",gray);

	//Mat bw;
	//adaptiveThreshold(~gray, bw, 255, CV_ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 15, -2);

	//imshow("binary", bw);
	//string test;

	//src = imread(filename);

	Mat dst;
	Mat tmp;
	Mat bw;
	Mat rsz;
	//Size size(800, 900);
	Size size(794, 1122);
	resize(src, rsz, size);


	cvtColor(rsz, tmp, CV_BGR2GRAY);
	adaptiveThreshold(~tmp, bw, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 15, -2);
	//bitwise_not(alpha, alpha);
	//imshow("stuff", bw);


	Mat horizontal = bw.clone();
	Mat vertical = bw.clone();

	int scale = 20;

	int horizontalsize = horizontal.cols / scale;

	Mat horizontalStrucuture = getStructuringElement(MORPH_RECT, Size(horizontalsize, 1));

	erode(horizontal, horizontal, horizontalStrucuture, Point(-1, -1));

	dilate(horizontal, horizontal, horizontalStrucuture, Point(-1, -1));

	//imshow("horizontal", horizontal);

	vector<Vec4i> lines;

	HoughLinesP(horizontal, lines, 1, CV_PI / 180, 600, 50, 10);

	size_t lines_size = lines.size();

	vector<int> labels;

	int numberOfLines = partition(lines, labels, isEqual);

	int test = numberOfLines;

	Mat cdst;

	rsz.copyTo(cdst);
	

	point *info = new point[100];

	*length = lines_size;

	for (size_t i = 0; i < lines.size(); i++)
	{
		Vec4i l = lines[i];
		line(cdst, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0, 0, 255), 3, CV_AA);		
		point *temp = new point();
		temp->xBegin = l[0];
		temp->yBegin = l[1];
		temp->xEnd = l[2];
		temp->yEnd = l[3];
		info[i] = *temp;
	}
	
	*aData = info;
	int test1 = 0;

	
}

//int main()
//{
//	
//	string filename = "file-page3.jpg";
//
//	Mat src = imread(filename);
//
//	if (!src.data)
//	{
//		cerr << "Problem loading image" << endl;
//	}
//
//	//imshow("src", src);
//
//	//Mat gray;
//
//	//if (src.channels() ==3)
//	//{
//	//	cvtColor(src, gray, CV_BGR2GRAY);
//	//}
//	//else
//	//{
//	//	gray = src;
//	//}
//
//	//imshow("gray",gray);
//
//	//Mat bw;
//	//adaptiveThreshold(~gray, bw, 255, CV_ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 15, -2);
//
//	//imshow("binary", bw);
//	//string test;
//
//	//src = imread(filename);
//
//	Mat dst;
//	Mat tmp;
//	Mat bw;
//	Mat rsz;
//	Size size(800, 900);
//	resize(src, rsz, size);
//
//
//	cvtColor(rsz, tmp, CV_BGR2GRAY);
//	adaptiveThreshold(~tmp, bw, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY,15,-2);
//	//bitwise_not(alpha, alpha);
//	imshow("stuff", bw);
//
//	
//	Mat horizontal = bw.clone();
//	Mat vertical = bw.clone();
//
//	int scale = 20;
//
//	int horizontalsize = horizontal.cols /scale;
//
//	Mat horizontalStrucuture = getStructuringElement(MORPH_RECT, Size(horizontalsize,1));
//
//	erode(horizontal, horizontal, horizontalStrucuture, Point(-1, -1));
//
//	dilate(horizontal, horizontal, horizontalStrucuture, Point(-1, -1));
//
//	imshow("horizontal", horizontal);
//
//	vector<Vec4i> lines;
//
//	HoughLinesP(horizontal, lines, 1, CV_PI / 180, 600, 50, 10);
//
//	size_t lines_size = lines.size();
//
//	vector<int> labels;
//
//	int numberOfLines = partition(lines, labels, isEqual);
//
//	int test = numberOfLines;
//
//	Mat cdst;
//
//	rsz.copyTo(cdst);
//
//	for (size_t i = 0; i < lines.size(); i++)
//	{
//		Vec4i l = lines[i];
//		line(cdst, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0, 0, 255), 3, CV_AA);
//	}
//
//	imshow("detected lines", cdst);
//
//	waitKey(0);
//}

